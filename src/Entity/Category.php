<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{

    const IMAGES_PATH = 'images/';
    const MIME_TYPES = ['image/jpeg', 'image/png'];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IllustrationCommentee", mappedBy="category")
     */
    private $illustrationCommentee;

    public function __construct(String $base64Image = "")
    {
        $this->illustrationCommentee = new ArrayCollection();
        $this->image = "";
        if ($base64Image !== "")
        {
            $this->setBase64Image($base64Image);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|IllustrationCommentee[]
     */
    public function getIllustrationCommentee(): Collection
    {
        return $this->illustrationCommentee;
    }

    public function addIllustrationCommentee(IllustrationCommentee $illustrationCommentee): self
    {
        if (!$this->illustrationCommentee->contains($illustrationCommentee)) {
            $this->illustrationCommentee[] = $illustrationCommentee;
            $illustrationCommentee->setCategory($this);
        }

        return $this;
    }

    public function removeIllustrationCommentee(IllustrationCommentee $illustrationCommentee): self
    {
        if ($this->illustrationCommentee->contains($illustrationCommentee)) {
            $this->illustrationCommentee->removeElement($illustrationCommentee);
            // set the owning side to null (unless already changed)
            if ($illustrationCommentee->getCategory() === $this) {
                $illustrationCommentee->setCategory(null);
            }
        }

        return $this;
    }

    public function setBase64Image(String $base64Image)
    {
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64Image);
        // we could add validation here with ensuring count( $data ) > 1
        $decodedImage = base64_decode($data[1]);
        // open the output file for writing
        $file = fopen('/tmp/tempImage', 'wb');
        fwrite($file, $decodedImage);
        // clean up the file resource
        fclose($file);
        $imageType = mime_content_type('/tmp/tempImage');
        // if mime type is allowed
        if (in_array($imageType, self::MIME_TYPES)){
            // if an image is already attached to the product ...
            if ($this->image !== "") {
                // ... delete it
                unlink($this->image);
            }
            // generate a unique filename
            $filename = md5(uniqid());
            // get extension e.g png
            preg_match('/.*\/(.*)/', $imageType, $matches);
            $extension = $matches[1];
            // construct file path + name + extension string
            $filePath = self::IMAGES_PATH . $filename . '.' . $extension;
            // move from template to our storage folder
            rename('/tmp/tempImage', $filePath);
            // save imageURI
            $this->image = $filePath;
        }
        return $this;
    }
}
